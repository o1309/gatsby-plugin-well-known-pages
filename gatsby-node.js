const path = require('path');
const fs = require('fs');

if (typeof String.prototype.replaceAll === "undefined") {
  String.prototype.replaceAll = function (match, replace) {
    return this.replace(new RegExp(match, 'g'), () => replace);
  }
}

const template = (wellKnownDirectory, file, pluginOptions) => {
    let content;
    try {
        content = fs.readFileSync(path.join(wellKnownDirectory, file), 'utf8');
    } catch (e) {
        reporter.panic('Unable to read file ' + file);
        process.exit(1);
    }
    if (!pluginOptions || !pluginOptions.pages || 'Array' !== pluginOptions.pages.constructor.name) {
        return content;
    }

    for (const pageOptions of pluginOptions.pages) {
        if (file === pageOptions.fileName && pageOptions.variables) {
            const keys = Object.keys(pageOptions.variables);
            for (const key of keys) {
                content = content.replaceAll(key, pageOptions.variables[key]);
            }
        }
    }

    return content;
}

exports.onPostBuild = ({reporter, store}, pluginOptions) => {
    reporter.info(
        'Adding .well-known pages'
    );

    const {program} = store.getState();

    const wellKnownDirectory = path.join(program.directory, '.well-known');
    if (!fs.existsSync(wellKnownDirectory)) {
        reporter.panic(`Missing directory at ${wellKnownDirectory}`);
        process.exit(1);
    }

    const publicDirectory = path.join(program.directory, 'public', '.well-known');

    try {
        const files = fs.readdirSync(wellKnownDirectory);
        for (const file of files) {
            reporter.info(`Templating .well-known/${file}`);
            const buildPath = path.join(publicDirectory, file);
            if (!fs.existsSync(publicDirectory)) {
                fs.mkdirSync(publicDirectory);
            }
            const content = template(wellKnownDirectory, file, pluginOptions);
            fs.writeFileSync(buildPath, content);
        }
    } catch (e) {
        reporter.panic(e);
        process.exit(1);
    }
};
